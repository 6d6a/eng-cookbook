\input texinfo   @c -*-texinfo-*-
@c ===========================================================================
@c
@c This file was generated with po4a. Translate the source file.
@c
@c ===========================================================================

@c %**start of header
@setfilename eng-cookbook.info
@settitle Руководство инженера
@c %**end of header
@copying
Руководство и примеры для инженеров

Копирайт @copyright{} 2021 Олег Пыхалов

Данное рукодство написано с помощью
@uref{https://www.gnu.org/software/texinfo/, GNU Texinfo}.

@end copying

@titlepage
@title Руководство инженера
@subtitle Руководство и примеры для инженеров
@author Олег Пыхалов <go.wigust@@gmail.com>
@page
@vskip 0pt plus 1filll
@insertcopying
@end titlepage

@c Output the table of the contents at the beginning.
@contents

@ifnottex
@node Top
@top Руководство инженера

@insertcopying
@end ifnottex

@menu
* Сети::
* RAID::
* Оглавление::     
@end menu

@node Сети
@chapter Сети

Убедитесь, что вы выполняете изменения на обоих @var{br1-mr14.intr} и
@var{sr1-dh507-508.intr}.  Заметьте, что upstream в Datahouse имеет меньшую
пропускную способность.

@section DDoS
@cindex ddos


@subsection Servicepipe
@cindex servicepipe

To announce a prefix from servicepipe and deannounce it from other
upstreams, add a network to the @var{ddos} prefix list.

@example
root@@br1-mr14.intr> show configuration policy-options prefix-list ddos
@end example

@subsection Blackhole
@cindex blackhole

Чтобы добавить IP адрес в blackhole, добавьте его в префикс лист
@var{blackhole}.

@example
root@@br1-mr14.intr> set policy-options prefix-list blackhole-prefixes 78.108.88.220/32
@end example

@c @example
@c root@@br1-mr14.intr> run show route advertising-protocol bgp 85.235.192.226
@c @end example

@node RAID
@chapter RAID

@section Adaptec
@cindex adaptec

@subsection Поиск диска

Для определения диска, который вам необходимо заменить, нужно узнать номер
канала и номер устройства:

@example
arcconf GETCONFIG 1

Reported Channel,Device(T:L)       : 0,6(6:0)
Reported Location                  : Connector 1, Device 2
@end example

@subsection Включить подсветку
@cindex identify

Включить подсветку на корзине с диском выполнив команду @code{arcconf
identify}, например:

@example
arcconf identify 1 device 0 6
@end example

потому что
@example
arcconf IDENTIFY 1 DEVICE <channel> <device>
@end example

@subsection 5ee
@cindex 5ee

Когда 5ee находится в состоянии compressed массив не защищен hot-spare
диском:
@example
arcconf getconfig 1

Protected by Hot-Spare : No
@end example

После замены диска необходимо добавить его как hot-spare и пересканировать
массив:
@example
arcconf setstate 1 device 0 6 hsp logicaldrive 0
arcconf RESCAN 1
@end example

После вы увидите что 5ee массив выполняет expand:
@example
arcconf getstatus 1

Current operation : Expanding
Status : In Progress
@end example

@node Оглавление
@unnumbered Оглавление

@printindex cp

@bye
